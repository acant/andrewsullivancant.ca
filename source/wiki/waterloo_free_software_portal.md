---
title: Waterloo Free Software Portal
category: Project Ideas
---

Collecting links and notes about free software projects and contributors in the
[Region of Waterloo](https://en.wikipedia.org/wiki/Regional_Municipality_of_Waterloo).
I hope that this collection will eventually evolve into a portal/aggregate for
[FLOSS](https://en.wikipedia.org/wiki/Free_and_open-source_software#FLOSS) related
activity in the region.

# Projects

...which have one or more contributors living in the region.

* [Anti-Prism](https://antiprism.ca)
* [AutoScreenCapture](https://sourceforge.net/projects/autoscreen/)
* [Barry](http://www.netdirect.ca/software/packages/barry)
* [ciso8601](https://github.com/closeio/ciso8601)
* [check_docker](https://github.com/timdaman/check_docker)
* [ChimeraOS](https://chimeraos.org/)
* [CASL Support CiviCRM Extension](https://civicrm.org/extensions/casl-support)
* [Curv](https://github.com/doug-moen/curv)
* [cstore\_fdw](https://github.com/citusdata/cstore_fdw)
* [CrySP](https://crysp.uwaterloo.ca/software/) has a number of projects
  - [NetMirage](https://crysp.uwaterloo.ca/software/netmirage/)
  - [Slitheen](https://crysp.uwaterloo.ca/software/slitheen/)
* [dhall-ruby](https://git.sr.ht/~singpolyma/dhall-ruby)
* [Digital Storytelling](https://civictechwr.github.io/storytelling/)
* [dont\_repeat\_for](https://github.com/jayelkaake/dont_repeat_for)
* [ember-polaris](https://github.com/smile-io/ember-polaris)
* [Google Calendar Helper](https://github.com/pnijjar/google-calendar-helpers)
* [Google Chrome](https://www.google.com/chrome/)
* [Kedge](http://kedgeproject.org/)
* [Kompose](http://kompose.io/)
* [MarkUs](https://cs.uwaterloo.ca/twiki/view/ISG/MarkUs)
* [MatterMost](https://about.mattermost.com/)
* [OhMyForm](https://ohmyform.com/)
* [OpenFoodNetwork, Canada](https://openfoodnetwork.ca/)
* [OpenSCAD](http://www.openscad.org/)
* [Poet Open Source](https://poetos.github.io/)
* [quartermaster](https://github.com/timdaman/quartermaster)
* [SadServers](https://sadservers.com/)
* [sidekiq-heartbeat_monitor](https://github.com/jayelkaake/sidekiq-heartbeat_monitor)
* [simple\_speed\_tester](https://github.com/jayelkaake/simple_speed_tester)
* [Soprani](https://soprani.ca/)
* [Tailwind CSS](https://tailwindcss.com/)
* [Tokumeico](https://github.com/tokumeico)
* [Working Centre Linux Project](http://wclp.sourceforge.net/)

# People

* [Adam Glauser](https://twitter.com/AdamGlauser)
* [Adam Wathan](https://adamwathan.me/)
* [Bob Jonkman](http://sn.jonkman.ca/bobjonkman)
* [Brad van der Laan](http://bradvanderlaan.ca/)
* [Charlie Drage](http://www.charliedrage.com/)
* [Charles McColm](http://charlesmccolm.com)
* [Chris Irwin](https://chrisirwin.ca/)
* [Christopher Vollick](http://psycoti.ca/0/)
* [Collin Mills](http://www.colinjmills.ca/)
* [Darcy Casselman](http://flyingsquirrel.ca/)
* [Doug Moen](https://github.com/doug-moen)
* [Ed Maste](https://twitter.com/ed_maste), Director of Project Development at
    [FreeBSD Foundation](https://www.freebsdfoundation.org/about/staff/)
* [Eric Gerlach](https://github.com/egerlach)
* [Eric Roberts](http://www.ericroberts.ca/)
* [Gavin Kendall](https://github.com/gavinkendall)
* [Hadi Moshayedi](https://www.linkedin.com/in/hadi-moshayedi-337198b5/)
* [Hubert Chathi](https://www.uhoreg.ca/)
* [Ian Ring](http://www.ianring.com/)
* [Jason Eckert](http://jasoneckert.net)
* [Joan Currie](https://github.com/JECurrie)
* [John Kerr](https://twitter.com/johnekerr)
* [John van Ostrand](https://twitter.com/jvanostrand)
* [Jonathan Fritz](http://www.jonathanfritz.ca/)
* [Justin Filip](https://jfilip.ca/)
* [Keefer Rourke](https://krourke.org/)
* [Khalid Baheyeldin](https://baheyeldin.com/)
  - [Drupal maintainer](http://cgit.drupalcode.org/drupal/tree/MAINTAINERS.txt?h=7.x) as [kbahey](https://www.drupal.org/u/kbahey) and [wafaa](https://www.drupal.org/u/wafaa)
* [Michael Hewson](https://github.com/mikeyhew)
* [Michael Overmeyer](https://movermeyer.com/open-source/)
* [Marc Pare](http://parentreprise.com/)
* [Marcel Gagne](http://marcelgagne.com/)
  - [Cooking with Linux](http://cookingwithlinux.com/) video tutorials
* [Marius Kintel](https://ca.linkedin.com/in/mariuskintel)
* [Paul Nijjar](http://pnijjar.freeshell.org/)
* [Ralph Janke](https://twitter.com/RalphJanke)
* [Raul Suarez](http://rarsa.suarez.ca/)
* [Stephen Paul Weber](https://singpolyma.net/)
* [Theo Belaire](https://csclub.uwaterloo.ca/~tbelaire/)
* [Tim Lawrence](https://github.com/timdaman)
* [Tristan Hume](http://thume.ca/)
* [Robert Elder](http://robertelder.org/)
* [Will Spatzel](http://spaetzel.com/)

## Alumni

* [Denver Gingerich](https://ossguy.com/)
* [Ilya Grigorik](https://www.igvita.com/)
* [Kyle Farwell](https://kfarwell.org)
* [Richard Weait](https://twitter.com/rweait)

## Research

You can also find people who list their locations on github:

* [Github - location: waterloo](https://github.com/search?utf8=%E2%9C%93&q=location%3Awaterloo&type=Users&ref=searchresults)
* [Github - location: kitchener](https://github.com/search?utf8=%E2%9C%93&q=location%3Akitchener&type=Users&ref=searchresults)

# Organizations

* [2bits](http://2bits.com/), Drupal related consulting services
* [Alteeve](https://alteeve.ca/)
* [Cryptography, Security, and Privacy (CrySP) Research Group](https://crysp.uwaterloo.ca/) ([software](https://crysp.uwaterloo.ca/software/))
* [IndieServe](http://www.indieservenetworks.com/), FLOSS friendly VPS
* [NetDirect](http://netdirect.ca)
* [PeaceWorks Technology Solutions](https://peaceworks.ca/)
* [Remote-Learner](https://www.remote-learner.com/)
* [Shopify](https://www.shopify.ca/) ([software](https://shopify.github.io/))
* [Sienci](http://sienci.com/)
* [Smile.io](https://smile.io/), formerly Sweet Tooth ([software](https://github.com/smile-io))
* [Vehikl](https://twitter.com/vehikl)

# Meetups

* [KWLUG](http://kwlug.org)

# Events

* [KW Linuxfest](http://kwlinuxfest.ca/)

## Close by

* [BSDCan](https://www.bsdcan.org/), Ottawa
* [GoCon Canada](https://gocon.ca/), Toronto
* [NorthSec](https://www.nsec.io/), Montreal
* [PGCon](https://www.pgcon.org/), Ottawa
* [True North PHP](https://truenorthphp.ca/), Toronto

# Groups which include the Region

* [LibrePlanet Ontario](https://libreplanet.org/wiki/Group:LibrePlanet_Ontario)
* [FSF Canada](http://fsf.ca)
