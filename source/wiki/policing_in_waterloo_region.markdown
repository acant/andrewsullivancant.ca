---
title: Policing in Waterloo Region
category:
  - Waterloo Region
---

# G20 related infiltration of activist groups
* [How police infiltrated groups planning G20 protests](https://www.theglobeandmail.com/news/politics/how-police-infiltrated-groups-planning-g20-protests/article4170473/), Adrian Morrow and Kim Mackrael in The Globe and Mail, 2011-11-22
* [Unmasked](https://maisonneuve.org/article/2013/03/6/unmasked/), Andrea Bennet,
2012-03-06
* [The Police #7 - The G20: Conspiracy](https://www.canadaland.com/podcast/the-police-7-the-g20-conspiracy/), Canadaland COMMONS, 2021-01-13

# See also
* [School Resource Officers in Waterloo Region](/wiki/school_resource_officers_in_waterloo_region)

