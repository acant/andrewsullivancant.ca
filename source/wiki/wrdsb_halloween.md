---
title: WRDSB Halloween
tags:
  - wrdsb
  - waterloo
  - halloween
---

* [WRDSB halloween tags](https://www.wrdsb.ca/blog/tags/halloween/)
* https://www.wrdsb.ca/blog/2014/10/31/happy-20th-halloween-pumpkin-patrol/, 2014-10-31
* https://www.wrdsb.ca/blog/2017/10/17/committed-to-student-belonging-on-halloween-and-everyday/, 2017-10-17
* https://news.ontario.ca/en/statement/58867/stay-safe-and-follow-public-health-advice-this-halloween, 2020-10-19
* https://jme.wrdsb.ca/2020/10/21/prioritizing-student-health-and-safety-during-halloween/, 2020-10-21
* https://lrw.wrdsb.ca/2020/10/21/prioritizing-student-health-and-safety-during-halloween/, 2021-10-21
* https://mof.wrdsb.ca/2020/10/21/prioritizing-student-health-and-safety-during-halloween/, 2021-10-21
* https://globalnews.ca/news/7414059/students-halloween-costumes-treats-waterloo-public-schools/, 2020-10-22
* https://kitchener.citynews.ca/all-audio/the-mike-farwell-show/wednesday-october-13th-2021-4518879, 2021-10-13
* https://kitchener.citynews.ca/local-news/no-halloween-celebrations-at-wrdsb-schools-4510063 , 2021-10-13
  - quotes the Mike Farwell show about this
  - https://www.reddit.com/r/TumblrInAction/comments/q7jpq9/no_halloween_celebrations_at_wrdsb_schools/
* https://kitchener.ctvnews.ca/wrdsb-schools-advised-not-to-take-part-in-halloween-celebrations-1.5621843, 2021-10-13
  - https://www.reddit.com/r/waterloo/comments/q8nk8s/wrdsb_schools_advised_not_to_take_part_in/
* https://jme.wrdsb.ca/2021/10/14/halloween-update/, 2021-10-14
* https://www.change.org/p/students-wrdsb-canceling-halloween-celebrations, 2021-10-14
* https://twitter.com/Karen_Meissner_/status/1448995466989944835, 2021-10-15
* https://twitter.com/ESL_fairy/status/1449326553045901312, 2021-10-16
* https://jenvasicwaterloo.ca/learn-more/2021/10/16/in-support-of-the-school-boards-decision-around-halloween, 2021-10-16
