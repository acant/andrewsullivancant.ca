---
title: Mastadon
---

[Mastadon](https://en.wikipedia.org/wiki/Mastodon_(software)) is an [ActivityPub](https://en.wikipedia.org/wiki/ActivityPub) based [microblogging](https://en.wikipedia.org/wiki/Microblogging) platform.

I finally started trying it out in 2023, after the [Acqusition of Twitter by Elon Musk](https://en.wikipedia.org/wiki/Acquisition_of_Twitter_by_Elon_Musk).

My current account on [mstdn.ca](https://mstdn.ca/) is: https://mstdn.ca/@andrewsullivancant

And I originally created an account on [wrmstdn.ca](https://wrmstdn.ca/), before it was shut down: https://wrmstdn.ca/@andrewsullivancant

# Other Mastadon Instances
* https://cosocial.ca/
* https://wrmstdn.ca/
  - shutdown now. It was setup by [Robert Bowerman](https://www.linkedin.com/in/rbowerman/)
* https://mstdn.ca/
* https://ruby.social/
* https://ottawa.place/
* https://canada.masto.host/
* https://hub.fosstodon.org/
* https://infosec.exchange/

# External Links
* http://linuxmafia.com/pipermail/sf-lug/2022q4/015730.html
* [Verifying Github on Mastadon](https://til.simonwillison.net/mastodon/verifying-github-on-mastodon)
* [Multi-tenancy with custom domains #2668](https://github.com/mastodon/mastodon/issues/2668)
* [Mastodon: A New Hope for Social Networking](https://tidbits.com/2023/01/27/mastodon-a-new-hope-for-social-networking/)
* [Moving a Mastodon account to another server](https://www.fabriziomusacchio.com/blog/2022-12-28-mastodon_server_migration/)

