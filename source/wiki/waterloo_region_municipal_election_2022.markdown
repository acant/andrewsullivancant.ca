---
title: Waterloo Region Municipal Election 2022
---

As part of the [2022 Ontario municipal elections](https://en.wikipedia.org/wiki/2022_Ontario_municipal_elections) Waterloo Region is [electing](https://en.wikipedia.org/wiki/2022_Waterloo_Region_municipal_elections) it's various levels of councillors and trustees.

Nominations closed on August 19th, 2022.

# External Links
* https://wrvotes.com/
* https://waterlooregionvotes.org/
  - implemented by [CivicTechWR](https://civictechwr.org/)
* http://poliblog.jonkman.ca/Poliblog-Elections/2022-10-24-Municipal-Election/
* https://citified.substack.com/p/municipal-election-round-up
* https://citified.substack.com/p/now-what
* https://en.wikipedia.org/wiki/2022_Ontario_municipal_elections
* https://www.wrdsb.ca/trustees/about/become-a-trustee/
* https://www.kitchener.ca/en/council-and-city-administration/running-for-office-in-kitchener.aspx
* https://www.therecord.com/news/waterloo-region/2022/08/20/four-waterloo-residents-seeking-mayors-job.html
* https://twitter.com/FABWaterloo/status/1552341211670843395, list of
  uncontested seats as of 2022-07-27
* https://twitter.com/JodiKoberinski/status/1562146836713988097, list of
  anti-lgbtq and anti-anti-racism WRDSB trustee candidates
* https://twitter.com/MmeKazemzadeh/status/1565420057735446530, another list of
  generally progressive trustee candidates
* https://www.knowyourwrdsbtrustees.ca/
* https://www.voteagainstwoke.ca/
  - [for public school board](https://www.voteagainstwoke.ca/plan-your-vote-step-3-epsb-waterloo-region-district-school-board)
  - [for Catholic school board](https://www.voteagainstwoke.ca/plan-your-vote-step-3-ecsb-waterloo-catholic-district-school-board)
* https://groundupwr.weebly.com/election-2022.html
* https://greaterkwchamber.com/municipal-election-candidate-forums/
* http://pnijjar.freeshell.org/2022/election-silencing/
* https://pressprogress.ca/anti-lgbtq-activists-are-networking-and-coordinating-to-take-over-canadian-school-boards/
* https://www.therecord.com/news/municipal-election/2022/10/27/the-culture-wars-will-continue-in-the-years-to-come-at-waterloo-regions-public-board.html
* http://pnijjar.freeshell.org/2022/wr-preblather/
* https://www.therecord.com/news/waterloo-region/opinion/2022/10/25/these-municipal-elections-made-history.html
