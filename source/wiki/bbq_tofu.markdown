---
title: BBQ Tofu
---

Ingredients for the tofu part:
==============================
* 1 block of tofu
* 2 medium onions
* 1 green pepper (fresh or frozen)
* 1 red pepper (fresh or roast)

Ingredients for the sauce part:
===============================
* 2 heaping tbsp peanut butter
* 1 tbsp vegetable oil
* 2 tsp mustard
* 4 tsp rice wine
* 2 cloves of garlic
* 4 tsp chili powder
* 2 tsp honey or molasses
* dash of hot sauce
* generous ground pepper

Instructions
============
TODO
