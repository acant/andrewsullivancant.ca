andrewcant.ca
=============

This is the [Middleman](http://middlemanapp.com) powered repository for [andrewsullivancant.ca](http://www.andrewcant.ca)

* [Backlog](https://gitlab.com/acant/andrewsullivancant.ca/blob/master/BACKLOG.otl)
* [Checklists]( https://gitlab.com/acant/andrewsullivancant.ca/blob/master/CHECKLISTS.md)

To Deploy
---------
This website is now deployed on [Gitlab Pages](https://docs.gitlab.com/ee/user/project/pages/)
by the [Gitlab CI](https://docs.gitlab.com/ee/ci/README.html). This happen on all
commits which are pushed to master and configured in
[.gitlab-ci.yml](https://gitlab.com/acant/andrewsullivancant.ca/blob/master/.gitlab-ci.yml).

Running Locally
---------------
To keep the local environment, and the Gitlab Pages build environment
consistent, please use `docker-compose` to run the middleman server locally.

```
docker-compose up
```

The website should now be available at http://localhost:4567 and will live-reload
changes as they are made to source files.

Framework Requirements
----------------------
Notes about choosing which framework to use to building my homepage. This will
be useful to comparing new frameworks to decide at what point I might want to
switch. My current requirements are:
* static generator
* FLOSS (GPL > MIT > other)
* Ruby is preferred
* HAML, SASS, Markdown support
* healthy project (relatively popular, good contribution base, good code
  quality)
* already used by others for Indieweb style web sites, would be nice

Alternative Frameworks
----------------------
* [Jekyll v2](http://jekyllrb.com/)
* [Octopress](http://octopress.org/docs/) (but the released version has not be
  updated to Jekyll v2 as of 2014-08-17)

Design Ideas
------------
* http://www.tutorialchip.com/inspiration/best-and-creative-personal-websites/
* http://sixrevisions.com/design-showcase-inspiration/responsive-webdesign-examples/
* http://socialdriver.com/2013/06/10/50-best-responsive-website-design-examples-of-2013/
* http://fizzle.co/sparkline/personal-domain-names
* http://everypageispageone.com/

Other nice looking personal sites:
* http://www.brainshave.com/
* http://tobyx.com/
* http://fmarier.org/
  - this is really nice and simple
  - and the author is in Debian, UW alumni, and works for Mozilla
* http://garretkeizer.com/
* http://spin.atomicobject.com/2015/08/24/learn-ember-js-quickly/
  - moves the link bar to the right in the widest mode
* http://danluu.com/
* https://gyrosco.pe/ and http://aprilzero.com/
* https://adactio.com/
* https://sarasoueidan.com/
* http://sarahdrasnerdesign.com/
* https://www.johanbrook.com/
* http://davidmiranda.info/
* https://mzucker.github.io/
* http://kellysutton.com/
* http://victoriablessing.com/
* https://www.igvita.com/
* https://sonniesedge.co.uk/
* https://mylesb.ca/
* https://michellebarker.co.uk/
* https://nivenly.com/
* https://brianlovin.com/
* https://maggieappleton.com/

SEO Guidelines
--------------
* http://katemats.com/what-every-programmer-should-know-about-seo/

Wiki Page Structures
--------------------
In the [/wiki] I am trying to make use of some of the patterns already
development on [Wikipedia](https://en.wikipedia.org/wiki/Wikipedia:Manual_of_Style/Layout)
* split articles when they get too large, and avoid creating lots of little
    pages
  - https://en.wikipedia.org/wiki/Wikipedia:Splitting
* use common sections
  - "See Also", which links to other pages in the wiki
  - "Notes and References", which should contain citations and footnotes
  - "External Links", which are not citations or included inline
