---
title: Dahl
---

Ingredients
===========

Instructions
============
* heat oil and butter in deep pan
* add onion, garlic, ginger, cumin seeds, mustard seeds, (optional) chillies and saute until the seeds pop
* add tomato, lentils, water, cumin, coriander, salt, pepper
* cook for approximately 30 minutes
* (optional) add a little lemon and cilantro
* serve with rice and yogurt

Original Recipe from []()
