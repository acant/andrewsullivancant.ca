---
title: Waterloo FLOSS Club
category: Project Ideas
---

Organize regular events in Waterloo region to contribute to open source
projects. This can include volunteer events and helping companies organize
time for their employees to contribute.

# Ways to contribute

* fixing bugs
* updating documentation
* translating documentation
* adding features
* helping to improve resumes
* triage existing issues
  - clarify the report or create test case
  - reproduce the error in your own environment
  - report that an issue has been resolved
* add screenshots to repositories
* fix Linux kernel drivers for hardware someone has

## Free Culture/Open Data contributions

If you do not want to program, you can contribute to FLOSS projects with
documentation, translation, or reproducing user facing bugs. You could also
contributed to some non-software free and open projects:

* add, update or translate Waterloo region related articles in [Wikipedia](https://www.wikipedia.org/)
* update map data at [OpenStreetMaps](https://www.openstreetmap.org/relation/2062151)

# Advice

* [Contributing to Your First Open Source Project](https://blog.devcenter.co/contributing-to-your-first-open-source-project-a-practical-approach-1928c4cbdae)
* [git-workflow](https://github.com/asmeurer/git-workflow)
* [How to Contribute to Open Source](https://github.com/FreeCodeCamp/how-to-contribute-to-open-source)
* [How to find you first open source bug to fix](https://medium.freecodecamp.com/finding-your-first-open-source-project-or-bug-to-work-on-1712f651e5ba)
* [A Beginner’s Very Bumpy Journey Through The World of Open Source](https://medium.freecodecamp.com/a-beginners-very-bumpy-journey-through-the-world-of-open-source-4d108d540b39)
* [Running First-time contributor Workshops](https://robots.thoughtbot.com/running-firsttime-contributor-workshops)
* [What open source project should I contribute to?](https://medium.com/@kentcdodds/what-open-source-project-should-i-contribute-to-7d50ecfe1cb4)
* [Contributing to Big Bad Open Source](https://robots.thoughtbot.com/contributing-to-big-bad-open-source)

## List of projects that need help

* [Code Shelter](https://www.codeshelter.co/)
* [Code Triage](https://www.codetriage.com), service which can suggest issues
  which need work
* [Docs Doctor](http://www.docsdoctor.org/)
* [Jazzband](https://jazzband.co/)
* [The League of Extraordinary Packages](https://thephpleague.com/)
* [Up for Grabs](http://up-for-grabs.net/#/tags/ruby)

## Guidelines from specific projects

* [django](https://docs.djangoproject.com/en/dev/internals/contributing/new-contributors/)
* [gitlab](https://gitlab.com/gitlab-org/gitlab-development-kit)
* [imfe](https://github.com/julianguyen/ifme)
* [moya](https://github.com/Moya/contributors)

# Similar Events and Projects

* [Hacktoberfest](https://hacktoberfest.digitalocean.com/)
* [HiveWR](http://hivewr.ca/)
* [Canada Learning Code](http://www.canadalearningcode.ca/)
* [Outreachy](https://www.gnome.org/outreachy/)
* SFLC grants?
* Google summer of Code
* Rails Girls
* Debian Summer of Code
* Mozilla Summer of Code
* [24 Pull Requests](https://24pullrequests.com/)
* [Canada Learning Code Week](http://www.canadalearningcode.ca/week/)

# Sources of Fundings/Grants
* [Shuttleworth Foundation](https://shuttleworthfoundation.org/)

* [A handy guide to financial support for open source](https://github.com/nayafia/lemonade-stand)
