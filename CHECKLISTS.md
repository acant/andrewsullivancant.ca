Checklists
==========

As described by [xkcd1205](https://xkcd.com/1205/) sometimes it is not work
automating a task. And sometimes it is better to start with a checklist, even
if that task might be automated eventually.

Profile image updates
---------------------
1. [ ] Find an awesome new photo with my face!
1. [ ] Make a new square crop for my face, and save the resulting file to *source/photo/%Y/%m-%d-1.jpg*
     * do not scale this version of the image, it should be as big as possible,
       so that it will not need to be scaled up when being used with other
       services
1. [ ] Convert the new image to the homepage avatar and favicon, using [imagemagick](https://www.imagemagick.org/):
       ```bash
       sudo apt-get install imagemagick
       convert source/photo/%Y/%m-%d-1.jpg -resize 80x80 source/avatar/_current.png
       convert source/photo/%Y/%m-%d-1.jpg -resize 80x80 source/avatar/current.jpg
       ```
1. [ ] Commit and push those files, so they will be deployed.
1. [ ] Update [Gravatar](https://en.gravatar.com/), which will also update:
     * [Github](https://github.com/)
     * [Gitlab](https://gitlab.com/)
     * [Slack](https://slack.com/)
     * [OpenStreetMap](https://www.openstreetmap.org/)
1. [ ] Update [Twitter](https://twitter.com/)
1. [ ] Update [Linkedin](https://www.linkedin.com/)
1. [ ] Update [Meetup](https://www.meetup.com/)
1. [ ] Update [Google](https://www.google.com/)
1. Done!

For future reference here is a summary of profile picture sizes from
[GoDaddy](https://www.godaddy.com/garage/facebook-profile-picture-size-and-more/):
| Service   | Profile Picture Size |
| --------- | -------------------- |
| Twitter   | 400px x 400px        |
| Facebook  | 170px x 170px        |
| Instagram | 110px x 110px (min.) |
| Pinterest | 160px X 160px        |
| LinkedIn  | 400px x 400px        |
| YouTube   | 800px x 800px**      |
| Google+   | 250px x 250px        |
| Medium    | 1400px x 1120px      |
| Tumblr    | 128px x 128px        |


