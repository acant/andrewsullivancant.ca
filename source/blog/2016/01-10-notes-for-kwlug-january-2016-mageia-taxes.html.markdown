---
:title: 'Notes for KWLUG January 2016: Mageia & Taxes'
:tags: kwlug
:date: 2016-01-10
---
[Marc Paré](https://twitter.com/MarcPar) presented on the
[Mageia](https://www.mageia.org/en/) linux distribution and talk about doing Canadian taxes using [Linux](http://www.linux.com/) and [Wine](https://www.winehq.org/). These are some of my notes from the night:

**Special mention: I was really impressed with Marc's organization. He started a Mageia install, then flipped to a finished install, then showed off some tax preparation, and finally returned to the first machine after the install had finished. Very smooth.**

Mageia
======

* Mageia is a community distribution forked from [Mandriva](http://www.mandriva.com/)
  - seems to be a similar relationship as between [RedHat](https://www.redhat.com/en) and [CentOS](https://www.centos.org/)
* mentioned [Rosa](http://www.rosalab.com/), which is also derived from Mandriva
* Mandriva had a difficult time when [Ubuntu](http://www.ubuntu.com/) come out and never really recovered
* Mandriva devs moved on to establish Mageia, as a community driven distribution
* Marc participated in the founding help writing their mission statement, and
  continues to help out with QA
* various architectures are supported
* although, they will be scaling back this support a bit for the next release
* Marc does tech support for about 40 people, who wanted to leave the
  [Windows](https://en.wikipedia.org/wiki/Microsoft_Windows) and he has successfully used Mageia for them
* he uses separate root and home partitions
  - keeps the OS in one place
  - makes it easy to format and re-install root, without loosing home data
  - has found it easier to recovered the data in home this way
* uses [TeamViewer](https://www.teamviewer.com/en/index.aspx) to remotely access his clients machines
* installs the default [KDE](https://www.kde.org/) and [LXDE](http://lxde.org/) as a fall back
* version 6 of Mageia is supposed to be release in 2 months
* [MCC](https://doc.mageia.org/mcc/4/en/content/index.html)(Mageia Control Centre) has been very successful for Marc's clients
* MCC makes it straight forward to add remote repositories after doing an
  install from CD
  - core and non-free are auto-selected
  - tainted repos are for packages that could have legal issues (mostly from a French point of view, as that is where the majority of Mageia's developers are located)
* some of the package that Marc installs during an install
  - mplayer
  - wine
  - flash
  - faad
    * comes from the tainted repository
  - livdvdcss2
    * used to be installed by default but recently moved into tainted
* Marc mentioned an issue about people accidentally moving or re-configuring
  this, which can cause confusion
  - I wonder if KDE can have its configuration locked down
* Marc suggested leaving network update of packages until after the initial
  install is finished

Tax Filing on Linux
=====================
* Marc is the [UFile in Wine](https://appdb.winehq.org/objectManager.php?sClass=application&iId=6630) maintainer, and is currently working on some issues
  in the 2015 release
* [Ufile](http://www.drtax.ca/en/UFile.aspx) has been friendly to Marc's Wine
  related requests in the past, but have seemed less interested recently.
  Perhaps related to their push to the web, instead of the desktop client?
* it has been necessary to install Microsoft fonts so that everything will be
  displayed correctly
* [CRA](http://www.cra-arc.gc.ca/menu-eng.html) has a legal limit of 20 returns for a non-professional
* apparently Canada has a 90% tax payment compliance rate, Australia in
  comparison has 60%
* Wine packages are updated pretty quickly for various distributions, so fixes will
  show up pretty quickly

Some comments from the audience
--------------------------
- [StudioTax](http://studiotax.com/) is a free tax package, mentioned from the audience
  * Marc mentioned that it is maintained by some ex-CRA employees located in [Ottawa](https://en.wikipedia.org/wiki/Ottawa)
  * also appears to be [supported in Wine](https://appdb.winehq.org/objectManager.php?sClass=application&iId=14915)
    but does not seem to have a current maintainer
- [ProFile](http://profile.intuit.ca/professional-tax-software/index.jsp)
  which is an example of software for Canadian tax professionals
