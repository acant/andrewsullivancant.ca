---
title: ASC Wiki
---

* [Home and Personal Networks](/wiki/home_and_personal_networks)
* [Data Furnace](/wiki/data_furnace)
* [Free/Libre Open Source Software](/wiki/free_libre_open_source_software)
* [Programming](/wiki/programming)
* [Recipes](/wiki/recipes)
* [Remote Work](/wiki/remote_work)
* notes about [Ruby](/wiki/ruby)
* [Mastadon](/wiki/mastadon)

# Ontario

* [Minimum Wages in Ontario](/wiki/minimum_wages_in_ontario)

# Waterloo Region

* [Policing in Waterloo Region](/wiki/policing_in_waterloo_region)
* [Waterloo Region Telecom](/wiki/waterloo_region_telecom)
* [Waterloo Region Municipal Election 2022](/wiki/waterloo_region_municipal_election_2022)

# Project Ideas

* [KW Dashboard](/wiki/kw_dashboard)
* [Waterloo FLOSS Club](/wiki/waterloo_floss_club)
* [Waterloo Free Software Portal](/wiki/waterloo_free_software_portal)
