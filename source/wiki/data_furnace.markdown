---
title: Data Furnace
---

A Data Furnace is an idea that was first described by a [paper](http://research.microsoft.com/pubs/150265/heating.pdf)
where computer resources are distributed to homes where the waste heat could be use to warm the home.

# Questions

* what work loads should be included
  - GPU coin mining
  - distributed storage
* initial costs
* energy costs for South-Western Ontario
* expected costs to upgrade hardware over time
* connectivity requirement and availability
* could hardware be systematically bought used/surplus
  - https://www.gcsurplus.ca/mn-eng.cfm

# Existing projects or products
* [Neralizer](https://www.nerdalize.com/)
* [Vaper.io Chamber](https://www.vapor.io/chamber/)
* [Quarnot](https://www.qarnot.com/)
* Project Exergy
  - seem to have been a DIY and kickstarter, but does not seem to be doing
      anything else now
  - https://www.networkworld.com/article/2872580/project-exergy-using-cloud-computing-to-heat-homes.html
  - https://rog.asus.com/forum/showthread.php?77916-My-Rig-in-Popular-Mechanics
  - https://forums.evga.com/m/tm.aspx?m=2400896&p=1
* [LeafCloud](https://www.leaf.cloud/)
* [Heata](https://www.heata.co/)
  - https://www.theregister.com/2023/01/12/heata_offers_free_hot_water/
  - https://www.tomshardware.com/news/heata-server-hot-water-trial-uk
* [Heatbit](https://heatbit.com/)
  - https://hackaday.com/2023/05/13/home-heating-with-bitcoin-miners-is-now-a-real-thing/

# Distributed computing/storage projects
* https://www.inrupt.com/
* https://golem.network/
* https://www.rendertoken.com/
* [Bitcoin](https://en.wikipedia.org/wiki/Bitcoin)
* [Ethereum](https://en.wikipedia.org/wiki/Ethereum)
* descriptions for modular ceph clusters with one CPU-per-HDD
  - https://www.ambedded.com.tw/en/product/ceph-storage-appliance.html
  - https://www.youtube.com/watch?v=4rY3yrE2ysQ&feature=emb_logo
* [GridCoin](https://gridcoin.us/)

# References

* https://en.wikipedia.org/wiki/Data_furnace
* https://news.ycombinator.com/item?id=15620010
* https://www.cloudandheat.com/en/private-infrastructure.html
* [KWLUG November 2017: Large-scale Open Source Storage and Filesharing](https://kwlug.org/node/1105)
* [6 GPU Ethereum Mining Rig Hardware Build Guide - Coin Mining Rigs](http://www.coinminingrigs.com/how-to-build-a-6-gpu-mining-rig/)
* [The era of the cloud’s total dominance is drawing to a close](https://www.economist.com/news/business/21735022-rise-internet-things-one-reason-why-computing-emerging-centralised)
* [Backblaze storage pod specs](https://www.backblaze.com/b2/storage-pod.html)
* [Hyper-converged infrastructure](https://en.wikipedia.org/wiki/Hyper-converged_infrastructure)
  - seems like a PR term, but it might contain some useful ideas
  - the important part seems to be about easily distributing computation
    between the cloud and local hardware
* https://www.fourmilab.ch/fourmilog/archives/2018-10/001787.html
* [ohmm Mining](https://upstreamdata.ca/)
  - project bitcoin from energy in vented gas in oil and gas projects in isolated locations
  - ideas seems to be that the gas must be vented, and there is no infrastructure to send the power back to the grid
  - but it there can be enough power to run their mining rig
* [Gaming PC vs. Space Heater Efficiency (2013)](https://www.pugetsystems.com/labs/articles/Gaming-PC-vs-Space-Heater-Efficiency-511/)
* description of a fanless desktop build with GPU
  - might be some useful part for adding heatsinks and heatpipes to GPUs
  - https://fabiensanglard.net/the_beautiful_machine/index.html
  - [Streacom](https://streacom.com/) might provide some product or design ideas
* [How I heat my home by mining crypto currencies](https://blog.haschek.at/2021/how-i-heat-my-home-by-mining.html), 2021-02-03
  - https://news.ycombinator.com/item?id=26235414
* [Mining Bitcoin for Heat, Strawberries and Chickens](https://www.coindesk.com/mining-bitcoin-heat-strawberries-chickens)
* [NVTC Presentation: The Data Center Transformation](https://datacenterfrontier.com/nvtc/)
* [AMD Ryzen beats Intel Core i7 as a heater (that's also a server)](https://www.theregister.com/2017/09/14/qarnot_drops_intel_adopts_amd/)
* [Dutch companies try warming homes with cloud servers](https://www.theregister.com/2015/03/25/dutch_companies_try_warming_homes_with_cloud_servers/)
* https://www.eevblog.com/forum/reviews/heat-your-home-with-the-cloud/
* https://loe.org/shows/segments.html?programID=15-P13-00010&segmentID=8
* https://www.cloudandheat.com/
* https://automationartist.com/crypto-heater/
