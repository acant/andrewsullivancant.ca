---
:title: Ruby version review 2016
:tags: gitdocs ruby version 2016
:date: 2016-11-05
---
While upgrading ruby gem dependencies for [gitdocs](https://github.com/nesquena/gitdocs)
I decided that I would not upgrade [activerecord](https://rubygems.org/gems/activerecord)
to v5.x in order to keep support for Ruby v2.0. This allow gitdocs to work with the
default ruby on most desktop operating systems.

The current state of [Ruby versions](https://www.ruby-lang.org/en/downloads/)
are:

* 2.3.1 and 2.2.5 are stable and supported
* 2.1.10 is in security maintenance, EOL soon
* 2.0.0 is EOL

Which suggests that dropping 2.0.0 support would be a good idea. However, gitdocs
is a user facing desktop/laptop kind of program so what Ruby versions can I expect
in that environment?

| Operating System                                                        | Ruby Version                                          |
| ----------------------------------------------------------------------- | ----------------------------------------------------- |
| [Debian 8.6 "jessie" (stable)](https://www.debian.org/releases/stable/) | [2.1.5](https://packages.debian.org/jessie/ruby)      |
| [Debian "sid" (unstable)](https://www.debian.org/releases/sid/)         | [2.3.0](https://packages.debian.org/sid/ruby)         |
| [Fedora 24-23](https://getfedora.org/)                                  | [2.4.8](https://apps.fedoraproject.org/packages/ruby) |
| [OS X El Capitan](https://en.wikipedia.org/wiki/OS_X_El_Capitan)        | 2.0.0                                                 |
| [OS X Sierra](https://en.wikipedia.org/wiki/OS_X_Sierra)                | 2.0.0                                                 |
| Windows with [RubyInstaller](http://rubyinstaller.org/)                 | 2.2.x (the suggested default)                         |

(N.B.: I have included Windows here and gitdocs *might* run on Windows, but it
has not been tested.)

Because of OSX I am going to hold off on dropping Ruby v2.0.0 support for now.
