---
:title: 'Notes for KWLUG October 2016: Tor Virtualization'
:tags: kwlug tor
:date: 2016-10-15
---
At the [KWLUG](http://kwlug.org/) meeting on [October 3 2016](http://kwlug.org/node/1042) Nik Unger spoke to us about his work emulating the [Tor network](https://www.torproject.org/) at [UWaterloo](https://uwaterloo.ca/) as part of the [Cryptography, Security, and Privacy (CrySP) Research Group](https://crysp.uwaterloo.ca/).

* CrySP publishes and contributes to various software projects at https://crysp.uwaterloo.ca/software/
* Nik's research focuses on secure messaging and private web browsing
* he discussed the reasons why privacy is important
  - mentioned the counter-argument ["If you have nothing to hide, you have nothing to fear"](https://en.wikipedia.org/wiki/Nothing_to_hide_argument)
  - however, there are many minor and incidental crimes (e.g., unknowingly buying
    a lobster which is little smaller than legal) which you could be guilty of
  - additional some rule break is necessary for social change. Many social and
    political changes we value could never have happend with perfect legal
    enforcement
   - privacy is a collective right as well as an individual right, where a
     perceived lack of privacy can [chill speech](https://en.wikipedia.org/wiki/Chilling_effect)     and reduce individual autonomy
* [Privacy Enhancing Technologies(PET)](https://en.wikipedia.org/wiki/Privacy-enhancing_technologies)
* [SSL/TLS](https://en.wikipedia.org/wiki/Transport_Layer_Security) protects
  communication content but not [metadata](https://en.wikipedia.org/wiki/Metadata)
* an [anonymity network](https://en.wikipedia.org/wiki/Proxy_server), like Tor,
  is intended to provide metadata protection
  - protects the user from their ISP or man-in-the-middle attacks
  - can optionally:
    * protect the identity of the service provider
    * prevent 3rd parties from knowing Tor is being used
* the bigger an anonymity network is the better protection it provides
* there are other anonymity networks too:
  - [VPN services](https://en.wikipedia.org/wiki/Virtual_private_network)
  - [I2P](https://geti2p.net/en/), focused on web browsing
  - [freenet](https://freenetproject.org/#), focused on storage
  - [DC-nets](https://en.wikipedia.org/wiki/Dining_cryptographers_problem),
    which are small and experimental
* Nik went through an explanation of a path through the Tor network which consists
  of 3 relays:
  - Guard (knows the user and the middle relay)
  - Middle (knows the guard and the exit)
  - Exit (knows the middle and the destination
* fewer relays provides obvious places for attack and more does not provide
  additional security

# Virtualizing Tor for research

* researchers want to run modified Tor to collect data or test changes **but**
  changing the real network can put real users at risk
* Tor provides [guidelines for research](https://blog.torproject.org/blog/ethical-tor-research-guidelines)
  which suggestrs using virtual networks whenever possible
* using real nodes can be a problem if researchers bring up a large number of
  nodes without declaring their ownership, result in a
  [Sybil attack](https://en.wikipedia.org/wiki/Sybil_attack) on the network
* [NetMirage](https://crysp.uwaterloo.ca/software/netmirage/) is the project
  that Nik is currently working on
  - it is a [C++](https://en.wikipedia.org/wiki/C++) re-write of an older Tor
    virtualization system from CrySP, written in [Python](https://www.python.org/)
    and [Bash](https://en.wikipedia.org/wiki/Bash_(Unix_shell)) to improve performance
  - they found the performance bottle neck was actually the API for the Linux
    Kernel Namespaces
  - this is currently a hard problem in the kernel, and sovling might be a future
    part of their research
  - they choose not to use an existing python project called [mininet](https://github.com/mininet/mininet/wiki/Introduction-to-Mininet) because:
    * it was more complex then they needed
    * and does not communicate directly with the kernel API, which was
      a performance concern
* [Linux Kernel Namespaces](https://en.wikipedia.org/wiki/Linux_namespaces)
  ([manpage](http://man7.org/linux/man-pages/man7/namespaces.7.html)) provide
  the building blocks for containerization
  - NetMirage uses the network namespace only
* 500 node network is about the limit, due to the performance bottleneck during
  setup
* once the setup is complete performance is only limited by kernel packet
  handling, so it is pretty fast
* challenges they have encountered which building NetMirage include:
  - debugging is difficult because [valgrind](http://valgrind.org/) does not
    support the kernel namespaces
  - using name spaces requires using various kernel APIs which have different
    conventions
