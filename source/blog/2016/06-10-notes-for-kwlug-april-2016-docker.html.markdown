---
:title: 'Notes for KWLUG April 2016: Docker'
:tags: kwlug docker
:date: 2016-06-10
---
[Tim Laurence](https://ca.linkedin.com/in/timlaurence) presented
a [docker](https://www.docker.com/)
workshop to [KWLUG](http://kwlug.org) on [April 4th 2016](http://kwlug.org/node/1017),

* there will be an Ubuntu release party on April 23rd

* this is a repeat of Tim's presentation to [KWRuby in March](http://www.meetup.com/kw-ruby-on-rails/events/228654199/) (which I also have some [notes on](/blog/2016-06-10-notes-for-kwruby-march-2016-docker))
* Tim reminded us that:
  - docker images are immutable
  - docker containers are mutable
* **docker images** size list is confusing because images are based on shared
  images, so list does not show real disk usage
* dockid is similar to a git commit hash, but it is just a random number
* dockid is the canonical id, and the names are just for readability
* **rmi** command to delete all images which are only used by the specified
  image
* Docker < [LXC](https://linuxcontainers.org/) < [OpenVZ](https://openvz.org/Main_Page)
* **run -rm** runs a container deleted on exist
* volumes can be mounted in multiple containers
* in Dockerfile **CMD** needs an absolute path, where **ADD** does not
