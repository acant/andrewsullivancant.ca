# frozen_string_literal: true

###
# Page options, layouts, aliases and proxies
###

page '/wiki/*', layout: :wiki

###
# Helpers
###

# Methods defined in the helpers block are available in templates
helpers do
  # Link with a FontAwesome icon included
  #
  # @param name [String]
  # @param href [String]
  # @param icon_name [String]
  # @param *flags [Array<Symbol>] optional flags
  # @param options [Hash]
  # @option options [Boolean] :rel_me (false}
  #
  # @return [String\
  def fa_icon_link_to(name, href, icon_name, *args)
    options = args.last.is_a?(Hash) ? args.pop : {}

    html_options = {}
    html_options[:rel] = 'me' if args.include?(:rel_me) || options[:rel_me]

    fa_style = 'fas'
    fa_style = 'fab' if icon_name.match?(/wikipedia|gitlab|github|twitter|linkedin|meetup|reddit/)

    link_to(
      %(<i class="#{fa_style} fa-#{icon_name}"></i>#{name}),
      href,
      html_options
    )
  end
end

# Automatic image dimensions on image_tag helper
# activate :automatic_image_sizes

# Reload the browser automatically whenever files change
activate :livereload

set :css_dir, 'stylesheets'

set :js_dir, 'javascripts'

set :images_dir, 'images'

# Build-specific configuration
configure :build do
  set :build_dir, 'public'
  activate :gzip
  activate :minify_css
  activate :minify_javascript
  activate :asset_hash

  activate :favicon_maker, icons: {
    'avatar/_current.png' => [
      { icon: 'favicon.ico', size: '64x64,32x32,24x24,16x16' }
    ]
  }

  # Use relative URLs
  # activate :relative_assets

  # Or use a different image path
  # set :http_path, "/Content/images/"
end

activate :blog do |blog|
  blog.permalink = 'blog/:year-:month-:day-:title'
  blog.sources   = 'blog/:year/:month-:day-:title.html'
  blog.paginate  = true
  blog.layout    = 'blog'
end

activate :drafts

set :markdown_engine, :redcarpet
set :markdown, fenced_code_blocks: true, autolink: true, with_toc_data: true, tables: true
activate :syntax

activate :directory_indexes

# HACK: Listing redirect here for the moment. Eventually I would like to
# convert this into a YAML file, or move into the frontmatter for the pages,
# redirect '/wiki/ruby_notes', to: '/wiki/ruby'
