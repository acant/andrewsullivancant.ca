---
title: First Post
date: 2010-02-02
---
Time to start my long neglected web presence.

My name is Andrew Sullivan Cant. I am a software developer living and working in [Kitchener-Waterloo](http://en.wikipedia.org/wiki/Kitchener-Waterloo).

I am an avid user of free and open source software and involved with various Free/Open software groups in area. This website is going to be a place to tie together my various points of presence on the web, and to start publishing some of the things that I am experimenting with.

Experiment #1: I am publishing this homepage using the [ikiwiki](http://ikiwiki.info) wiki compiler. From ikiwiki's website:

>Ikiwiki is a **wiki compiler**. It converts wiki pages into HTML pages suitable for publishing on a website. Ikiwiki stores pages and history in a [revision control system](http://ikiwiki.info/rcs/) such as [Subversion](http://ikiwiki.info/rcs/svn/) or [Git](http://ikiwiki.info/rcs/git/). There are many other [features](http://ikiwiki.info/features/), including support for [blogging](http://ikiwiki.info/blog/), as well as a large array of [plugins](http://ikiwiki.info/plugins/).

I am still learning ikiwiki's features and how to arrange things to my liking. So expect changes for the moment.

Finally, the site also gives me a place to put my [resume](resume). If you are an employer in the KW area looking for a programmer who likes to solve problems by integrating and extending open source software please contact me at [acant@alumni.uwaterloo.ca](mailto:acant@alumni.uwaterloo.ca).
