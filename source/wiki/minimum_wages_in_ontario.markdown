---
title: Minimum Wages in Ontario
category:
  - Ontario
---

Collection of links and information about minimum wage in Ontario. I hope to
include predictions about minimum wages changes, with data about the eventual
results.

# References
* https://boingboing.net/2018/10/28/fight-for-15.html
  - include claims before changes, and the actual results
* living wage data for WaterlooRegion
  - https://thecord.ca/waterloo-region-increases-new-minimum-wage-for-2018/
  - https://www.ontariolivingwage.ca/living_wage_by_region
  - https://duckduckgo.com/?q=waterloo+region+estimated+living+wage&t=ffab&ia=web
* https://north99.org/2018/11/01/fact-check-ford-said-ontario-lost-jobs-from-a-higher-minimum-wage-statistics-canada-says-thats-not-true/
* https://twitter.com/arindube/status/1115065162464100353
* https://north99.org/2018/11/01/fact-check-ford-said-ontario-lost-jobs-from-a-higher-minimum-wage-statistics-canada-says-thats-not-true/
* https://www.economist.com/schools-brief/2020/08/15/what-harm-do-minimum-wages-do
* [Canadian-born David Card among 3 winners of Nobel in economics](https://www.cbc.ca/news/world/nobel-economics-1.6207162)
  - [David Card](https://en.wikipedia.org/wiki/David_Card)
  - [Myth and Measurement: The New Economics of Minimum Wage](https://archive.org/details/mythmeasurement00davi) by David Card and [Alan Krueger](https://en.wikipedia.org/wiki/Alan_Krueger)
* [Kids in the Hall](https://en.wikipedia.org/wiki/The_Kids_in_the_Hall) Wages
  - https://imgur.com/gallery/FXVYUKT
  - https://www.youtube.com/watch?v=tFTuJMbWya0
* [Arindrajit Dube](https://en.wikipedia.org/wiki/Arindrajit_Dube) reserach who has compared minimum wage effects across state boundaries
  - effects on unemployment seem to have been minimal
  - https://arindube.com/
