---
title: Home and Personal Networks
---

Notes about building and maintaining [home](https://en.wikipedia.org/wiki/Home_network) and [personal networks](https://en.wikipedia.org/wiki/Personal_area_network). This is also intended to include how those networks might work with other services, to provide personal computing.

# Used and Refurbished Hardware

## Guides
* https://tedium.co/2019/06/04/used-workstation-computer-buying-strategy/

## Supplier in Waterloo Region
* [Computer Recycling](https://www.theworkingcentre.org/computer-recycling/178) at the [Working Centre](https://www.theworkingcentre.org/)
* [Eco-Tech Recycling](https://eco-techrecycling.com/sales)
* [Orion One](https://www.orionone.ca/shop)
* [Refurbit](https://www.refurbit.com/)
* [Tech Wreckers](https://www.techwreckers.ca/)
* [KW Laptops & Smartphones](http://www.waterloolaptops.ca/en/)

# See also
* [Data Furnace](/wiki/data_furance)
* [Waterloo Region Telecom](/wiki/waterloo_region_telecom)

# External Links
* https://blog.cyplo.dev/posts/2020/12/setup/
* https://giuliomagnifico.blog/networking/2022/08/14/home-network_v3.html
* https://friendlyelec.com/, which sell mini-routers based on OpenWRT
* https://wiki.friendlyelec.com/wiki/index.php/Main_Page
* https://www.waveform.com/tools/bufferbloat?test-id=cccdda5b-717f-40bf-a1c4-087a5cc2a2e4, 
