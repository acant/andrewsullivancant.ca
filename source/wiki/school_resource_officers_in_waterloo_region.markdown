---
title: School Resource Officers in Waterloo Region
category:
  - Waterloo Region
  - Waterloo Region District School Board
  - Policing in Waterloo Region
---

In 2020 [Waterloo Region District School Board(WRDSB)](https://en.wikipedia.org/wiki/Waterloo_Region_District_School_Board) started a review of its [School Resource Officer](https://en.wikipedia.org/wiki/School_resource_officer) program. This page is going to try and collect information about that program, as well as keeping track of the timeline. It focus on will the WRDSB, but also collect more general information about SRO programs in other places.

# Program stopped as of June, 21 2021

This SRO program has been stopped as of the [June 21, 2021 board meeting](https://www.wrdsb.ca/wp-content/uploads/2021-06-21-COW-Package.pdf). This meeting included some [supporting](https://docs.google.com/presentation/d/1cX3GOU7xUL-i9r6NA0KaZnCFUsNgE77R_IMJ-Io8CiM/edit#slide=id.p11) [slides](https://docs.google.com/presentation/d/1g8sn2RKSXTj8_G_fatpAx-OtHIwH65n9m_fISNm9K2M/edit#slide=id.p3). However, the contents of these slides was still aspirational, with no data on if the goals/aspirations were actually achieved.

# Questions

* where are the SRO agreement with the boards?
  - "School Police Protocol"
* do the other Regional boards have SRO programs, and what is their state
* when exactly did the programs start? (WRDSB program is about 20 years old?)
  - who has voted for the program over its history?
* what discussions and votes have been taken about the program?
  - find the existing published data
  - consider if there is more information to be filled in with Access to
    Information requests
* how much does it cost?
* did any current trustees participate? (Mike Ramsay seems to have been
  a trustee for most or all of the program)
* is it possible to correlate police data with SRO activity
* what is the history of other services in schools, nurses, counsellors, etc.
  - [Press Progress: Schools in Waterloo, Ontario Assigned Only One Nurse For Every 2,500 Students in the Middle of a Pandemic](https://pressprogress.ca/schools-in-waterloo-ontario-assigned-only-one-nurse-for-every-2500-students-in-the-middle-of-a-pandemic/)

# Timeline

When exactly did the program start, when ever the critical meetings, and are
there any important related events?

* when did the program start?
* :? board votes to temporarily suspend the program
* 2021-06-21: board votes to end the SRO program


# Link from Waterloo Region

* [CBC: Public school board's police resource officer review committee seeks volunteers](https://www.cbc.ca/news/canada/kitchener-waterloo/school-resource-office-program-waterloo-region-1.5834245), 2020-12-09
* [The Record: Attack on Truste Ramsay Won't Help Black Activists](https://www.therecord.com/news/waterloo-region/opinion/2020/12/16/attack-on-trustee-ramsay-wont-help-black-activists.html), 2020-12-16
  - https://twitter.com/Trustee_Ramsay/status/1339294878719750150
* [CBC: Police won't be in Waterloo region public schools until after review, says board](https://www.cbc.ca/news/canada/kitchener-waterloo/school-resource-officer-wrdsb-1.5614012), 2020-06-16
* [CBC: School resource officer program at WRDSB to undergo review](https://www.cbc.ca/news/canada/kitchener-waterloo/school-resource-officer-program-wrdsb-review-1.5769021), 2020-10-20
* [WRPS: Youth Programs](https://www.wrps.on.ca/en/our-community/youth-programs.aspx)
* [quotes from WRDSB students about SRO](https://twitter.com/judahoudshoorn/status/1408406022633447431)
* [WRDSB Cancellation of Resource Officer Program Driven by 'politics and not policy':police board member](https://kitchener.citynews.ca/local-news/wrdsb-cancellation-of-resource-officer-program-driven-by-politics-and-not-policy-police-board-member-4535583)
    - https://twitter.com/farwell_WR/status/1451298383252148224
* https://twitter.com/ScottPiatkowski/status/1407178321679589380
* https://twitter.com/ScottPiatkowski/status/1408126414927319047
* [response from Chief Larkin](https://twitter.com/BenEppelPress/status/1407407151178522637)
* [InsideWaterloo: The Politics of the SRO Program and Why It Had To End](https://insidewaterloo.ca/the-politics-of-the-sro-program-and-why-it-had-to-end/)
* [WRPS report disproportionate use of force against Black people](https://kitchener.citynews.ca/police-beat/wrps-report-disproportionate-use-of-force-against-black-people-2784228)
* ['We're shortchanging our students,' says school board trustee on cancelling SRO program](https://kitchener.citynews.ca/local-news/were-shortchanging-our-students-says-school-board-trustee-on-cancelling-sro-program-3902872)
* [WRDSB trustee won't be removed from school resource officer committee](https://kitchener.citynews.ca/local-news/wrdsb-trustee-wont-be-removed-from-school-resource-officer-committee-3198340)
* [Public board trustees vote to end police-in-schools program in Waterloo Region](https://www.therecord.com/news/waterloo-region/2021/06/22/public-board-trustees-vote-to-end-police-in-schools-program-in-waterloo-region.html)

# Links from elsewhere in Ontario

* [OHRC: Disproporinate Impact in Ontario](http://www.ohrc.on.ca/en/ontario-safe-schools-act-school-discipline-and-discrimination/vii-disproportionate-impact-ontario)
* [Upper Grand District School Board has voted to remove police from schools in Guelph, Wellington, and Dufferin County.](https://twitter.com/CTVKitchener/status/1387361016435126272)
* [Race and Criminal Injustice: New report from CABL, Ryerson’s Faculty of Law and the University of Toronto confirms significant racial differences in perceptions and experiences with the Ontario criminal justice system](https://cabl.ca/race-and-criminal-injustice-new-report-from-cabl-ryersons-faculty-of-law-and-the-university-of-toronto-confirms-significant-racial-differences-in-perceptions-and-experiences-with-the-ontari/)
  - [Little done to stop anti-Black racism in policing, criminal justice in last 25 years: Ontario report](https://globalnews.ca/news/7634725/report-anti-black-racism-racial-bias-ontario-criminal-justice-system/)

# Links from outside Ontario

* [Abolishing School Resource Officers Amidst the Black Lives Matter Movement: A History and Case Study in Oakland and Los Angeles](https://jpia.princeton.edu/news/abolishing-school-resource-officers-amidst-black-lives-matter-movement-history-and-case-study)
* [How Oakland Unified School District got its own police force](https://oaklandside.org/2020/06/23/how-oakland-unified-school-district-got-its-own-police-force/)
* [School Policing was Designed to Criminalize Black Students. We Must Follow Black Voices Calling for its Abolition](https://harvardcrcl.org/school-policing-was-designed-to-criminalize-black-students-we-must-follow-black-voices-calling-for-its-abolition/)
* [The Status of School Resource Officers in Walnut Creek](https://laslomaspage.org/2020/10/06/the-status-of-school-resource-officers-in-walnut-creek/)
* [‘When you come to school, you forfeit your rights’](https://scalawagmagazine.org/2021/01/student-resource-officers-nc/)
* [Wake County promised to reconsider cops in schools. When they didn't, students took to the streets.](https://scalawagmagazine.org/2021/01/student-resource-officers-nc-part-2/)
* [Vancouver Police Tried to Gift Toy Guns to Elementary School Students, Teacher Alleges During Public Forum](https://pressprogress.ca/vancouver-police-tried-to-gift-toy-guns-to-elementary-school-students-teacher-alleges-during-public-forum/)


