---
title: 'Townhouse Neighbourhoods in OpenStreetMaps'
# date: TBD When publishing
tags:
---

# Townhouse Neighbourhoods in OpenStreetMaps

* https://wiki.openstreetmap.org/wiki/Key:addr:place
* maybe the way that I have done it with the town house area gets
  - addr:housenumber
  - addr:street
  - name
  - place neighbourhood
    * https://wiki.openstreetmap.org/wiki/Tag%3Aplace%3Dneighbourhood
* then the individual houses get
  - addr:place
  - addr:housenumber
