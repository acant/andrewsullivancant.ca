---
:title: Bikes on Sidewalk By-laws in Waterloo Region
:tags: waterloo 2020 bikes sidewalks politics
:date: 2020-11-19
---
In August [John Gazzola](https://www.kitchener.ca/en/city-services/councillor-john-gazzola.aspx), councillor for Kitchener Ward 3, opposed the protected bike lane pilot project and suggested that bike riders should just ride on the sidewalk.

This was is original tweet

His original tweet was [here](https://twitter.com/jgaz3/status/1294676079060365313):

<blockquote class="twitter-tweet" cite="https://twitter.com/jgaz3/status/1294676079060365313?ref_src=twsrc%5Etfw"><p lang="en" dir="ltr">In an effort to more fully explain my thoughts on cycling let me share my experiences. I have been cycling on Westmount Rd for the last 18-20 years. It is a very busy street with miles &amp; miles of sidewalks on each side of it. I continue to ride on the sidewalk. No harm to pedest.</p>&mdash; John Gazzola (@jgaz3) <a href="https://twitter.com/jgaz3/status/1294676079060365313?ref_src=twsrc%5Etfw">August 15, 2020</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 

And Councillor Gazzola repeated the same idea later in the [Kitchener Citizen](http://www.kitchenercitizen.com/citycouncillorcolumns.html), [Matt Rodrigues](https://twitter.com/mattjrodrigues) clipped the article [here](https://twitter.com/mattjrodrigues/status/1299016426741813248):

<blockquote class="twitter-tweet" cite="https://twitter.com/mattjrodrigues/status/1299016426741813248?ref_src=twsrc%5Etfw"><p lang="en" dir="ltr">Fun. (p.s. it’s still *checks notes* against the by-law to ride on the sidewalk as an adult) <a href="https://t.co/rAcUOCGQdl">pic.twitter.com/rAcUOCGQdl</a></p>&mdash; Matt Rodrigues (@mattjrodrigues) <a href="https://twitter.com/mattjrodrigues/status/1299016426741813248?ref_src=twsrc%5Etfw">August 27, 2020</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

I thought that this was against local by-laws in Waterloo Region, but I was
unsure at the time. After some follow up it turns out that it is against the by-laws in every municipality, with the following exceptions:
* I could not find the Wellesley traffic by-laws
* North Dumfries does not include anything other than a definition for
  "sidewalks"

## By-laws

For my own future reference I have collected the links and quoted the appropriate by-laws

### [Kitchener](https://www.kitchener.ca/en/in-your-neighbourhood/community-bylaws.aspx)

> **PART II - DEFINITIONS**
>
> ...
>
> **Bicycle** includes a tricycle having a wheel or wheels of more than 50 centimetres in diameter.
>
> ...
>
> **Pedestrian** means any person afoot, any  person in a wheelchair, any child in a wheeled carriage, and any  person riding a non -motorized bicycle with wheels less  than 50 centimetres in diameter.
>
> ...
>
> **PART IV -GENERAL**
> <ol>
>    <li value="1">
>      Operation of Vehicles
>      <ol>
>        <li value="1" type="a">
>          Driving_On a Boulevard Sidewalk or Multi -Use Trail
>          <ol>
>            <li value="2" type="i">No person shall ride a bicycle on any  boulevard  or sidewalk  except at a driveway.</li>
>          </ol>
>        </li>
>      </ol>
>    </li>
> </ol>
>
> <cite>
>    <a href="https://www.kitchener.ca/en/resourcesGeneral/Documents/DSD_TRANSPORT_Traffic_and_Parking_Bylaw.pdf">
>      BY-LAW NUMBER 2019-113 OF THE CORPORATION OF THE CITY OF KITCHENER A by-law to regulate traffic and parking on highways  under the jurisdiction of the Corporation  of the City of Kitchener.
>    </a>
> </cite>

### [Cambridge](https://www.cambridge.ca/Modules/Bylaws/Bylaw/Search)

> **Sidewalks:** Sidewalk cycling is not permitted in Cambridge. This can be dangerous especially when crossing intersections and driveways. A bicycle is defined as a vehicle under traffic laws.
>
> <cite>
>   <a href="https://www.cambridge.ca/en/learn-about/cycling.aspx">Cycling</a>
> </cite>

### [North Dumfries](https://www.northdumfries.ca/en/township-services/common-by-laws.aspx)

> "Sidewalk" shall mean any municipal walkway or road works for the accommodation of pedestrians on that portion of a highway between the property line and the edge of the roadway
>
> <cite>
>   <a href="https://www.northdumfries.ca/en/township-services/resources/Documents/By-law-No.-3093-19-Regulate-and-Control-Operations-in-the-Township-Road-Use-By-law.pdf">
>     THE CORPORATION OF THE TOWNSHIP OF NORTH DUMFRIES BY-LAW NUMBER 3093-19 BEING A BY-LAW TO REGULATE AND CONTROL OPERATIONS IN THE TOWNSHIP OF NORTH DUMFRIES
>   </a>
> </cite>

### [Waterloo](https://www.waterloo.ca/en/living/bylaws-and-enforcement.aspx)

> **PART II - Definitions**
>
> ...
>
> <ol>
>   <li value="23">
>     “pedestrian” means any person afoot, any person in a wheelchair, any child in a wheeled carriage,  and  any  person  riding  a  non-motorized bicycle  with  wheels  less  than 50 centimetres in diameter;
>   </li>
> </ol>
>
> ...
>
> **Part IV -General**
> <ol>
>   <li value="1">
>     Operation of Vehicles
>     <ol>
>       <li value="1" type="a">
>         Driving on a Boulevard, Sidewalk or Multi-Use Trail
>         <ol>
>           <li value="2" type="i">
>             No  person  shall  drive  a  bicycle  having  a  wheel  or  wheels  more  than  50 centimetres in diameter or ride a skateboard on any boulevard or sidewalk except on a driveway.
>           </li>
>         </ol>
>       </li>
>     </ol>
>   </li>
> </ol>
>
> <cite>
>   <a href="https://www.waterloo.ca/en/government/resources/Documents/By-law/Traffic-and-parking-bylaw.pdf">
>     CITY OF WATERLOO BY-LAW NO. 08 –077BEING A BY-LAW TO REGULATE TRAFFIC AND PARKING ON HIGHWAYSUNDER THE JURISDICTION OF THE CITY OF WATERLOO
>   </a>
> </cite>

### [Waterloo Region](https://www.regionofwaterloo.ca/en/regional-government/by-law-enforcement.aspx)


> **PART II - Definitions**
>
> ...
>
> <ol>
>   <li value="25">
>     “pedestrian” means any person afoot, any person in a wheelchair, any  child  in  a  wheeled  carriage,  and  any  person  riding  a    bicycle that  is  not  a  motor-assisted  vehicle  with  wheels  less  than  50 centimetres in diameter
>   </li>
> </ol>
>
> ...
>
> **Part IV -General**
> <ol>
>   <li value="1">
>     Operation of Vehicles
>     <ol>
>       <li value="1" type="a">
>         Driving on a Boulevard, Sidewalk or Multi-Use Trail
>         <ol>
>           <li value="4" type="i">
>             No person shall drive a bicycle having a wheel or wheels more than 50 centimetres in diameter on any sidewalk except on a driveway or to directly supervise a child riding a bicycle having wheels not more than 50 centimetres in diameter
>           </li>
>         </ol>
>       </li>
>     </ol>
>   </li>
> </ol>
>
> <cite>
>   <a href="https://www.regionofwaterloo.ca/en/resources/Bylaws/By-law-16-023.PDF">
>     By-Law Number 16-023 of The Regional Municipality of WaterlooA By-law toRegulate Traffic and Parking on Highways Under the Jurisdiction of the Regional Municipality of Waterloo and toRepeal By-law 06-072
>   </a>
> </cite>

### [Wellesley](https://www.wellesley.ca/en/living-here/by-law-enforcement.aspx)

I could not find the by-laws for Wellesley.

### [Wilmont](https://www.wilmot.ca/en/township-office/By-laws.aspx)

> **Part II - Definitions**
>
> ...
>
> <ol>
>   <li value="28">
>     “pedestrian” means any person afoot, any person in a wheelchair, any child in a wheeled carriage, and any person riding a bicycle that is not a motor-assisted vehicle with wheels less than 50 centimetres in diameter;
>   </li>
> </ol>
>
> ...
>
> **Part IV - General**
> <ol>
>   <li value="1">
>     Operation of Vehicles
>     <ol>
>       <li value="1" type="a">
>         Driving on a Boulevard, Sidewalk or Multi-Use Trail
>         <ol>
>           <li value="2" type="i">
>             No person shall drive a bicycle having a wheel or wheels more than 50 centimetres in diameter on any sidewalk, boulevard or multi-use trail, except on a driveway or to directly supervise a child riding a bicycle having wheels not more than 50 centimetres in diameter;
>           </li>
>         </ol>
>       </li>
>     </ol>
>   </li>
> </ol>
>
> <cite>
>   <a href="https://www.wilmot.ca/en/township-office/resources/Documents/Traffic-and-Parking-By-law-Final-2.pdf">
>     THE CORPORATION OF THE TOWNSHIP OF WILMOT BY-LAW NO. 2016-52 A BY-LAW TO REGULATE TRAFFIC AND PARKING ON TOWNSHIP HIGHWAYS
>   </a>
> </cite>

### [Woolwich](https://www.woolwich.ca/en/township-services/By-Laws.aspx)

> **PART II - Definitions**
>
> ...
>
> <ol>
>   <li value="23">
>     **Pedestrian** means any person afoot, any person in a wheelchair, any child in a wheeled carriage,  and  any  person  riding  a  non-motorized bicycle  with  wheels  less  than 50 centimetres in diameter;
>   </li>
> </ol>
>
> ..
>
> <ol>
>   <li value="33">
>     **Sidewalk** means that part of a highway with a surface improved with asphalt,concrete or gravelfor the use of pedestrians.
>   </li>
> </ol>
> ...
> **PART IV -GENERAL**
> <ol>
>   <li value="1">
>     Operation of Vehicles
>     <ol>
>       <li value="1" type="a">
>         Driving on a Boulevard, Sidewalk or Multi-Use Trail
>         <ol>
>           <li value="2" type="i">
>             No  person  shall  drive  a  bicycle  having  a  wheel  or  wheels  more  than  50 centimetres in diameter or ride a skateboard on any boulevard or sidewalk except on a driveway.
>           </li>
>         </ol>
>       </li>
>     </ol>
>   </li>
> </ol>
>
> <cite>
>   <a href="https://www.woolwich.ca/en/township-services/resources/By-laws/CONSOLIDATED_TRAFFIC_AND_PARKING_BY-LAW_-_as_of_September_2020_By-law_56-2020_MAIN_BY-LAW_-_MASTER.pdf">
>     TOWNSHIP OF WOOLWICHBY-LAW NO. #70-2006A BY-LAW TO REGULATE TRAFFIC AND PARKINGON HIGHWAYS UNDER THE JURISDICTIONOF THE TOWNSHIP OF WOOLWICH ANDTO REPEAL BY-LAW NO. 45-2002
>   </a>
> </cite>

Well, that was...fun.

One day, maybe we can hope for by-laws which are easier to link and reference and not be buried in PDFs.
