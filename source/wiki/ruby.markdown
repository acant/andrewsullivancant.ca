---
title: Ruby
---

Various notes, links, and information about the [Ruby programming language](https://www.ruby-lang.org/en/) ([wikipedia](https://en.wikipedia.org/wiki/Ruby_(programming_language))).

Guides and Tutorial
===================
* [Pragmatic Programmers, first edition](http://ruby-doc.com/docs/ProgrammingRuby/)
  - this is a freely available version, which covers Ruby 1.6
  - the [Pragmatic Bookshelf has upto date Ruby books](https://pragprog.com/categories/ruby-and-rails/) on sale
* [Learning Ruby the Hardway](https://learncodethehardway.org/ruby/)
* [Ruby on Rails Guides](https://guides.rubyonrails.org/)
* [Rails Tutorial](https://www.railstutorial.org/), from [Michael Hartl](https://www.michaelhartl.com/)

Feature Flags
=============
[Feature flags](https://en.wikipedia.org/wiki/Feature_toggle) can be used to enable and
disable code dynamically. This makes it easier to do [Continuous Deployment](https://en.wikipedia.org/wiki/Continuous_delivery) since a new feature can be enabled in a limited manner.

There are a variety of gems which implement features flags in various ways:

* https://github.com/jamesgolick/rollout
* https://github.com/pda/flip
* https://github.com/pandurang90/feature_flags
* https://github.com/gmontard/helioth
* https://github.com/ckdake/setler
* https://github.com/MongoHQ/mongoid-feature-flags
* https://github.com/grillpanda/dolphin
* https://github.com/qype/feature_flipper

Sources and Discussion
----------------------
* http://stackoverflow.com/questions/4995556/ruby-feature-switches-feature-flippers
* http://www.stakelon.com/2012/01/simple-feature-flags-for-rails-mongoid/

Invariants and Assertions
=========================
[Software Assertions](https://en.wikipedia.org/wiki/Assertion_(software_development)) are statements in a routine which should always be true. There statements may or may not be disabled in production.

A simple version of this could be implemented just by raising exceptions.
However there are gems which allow for better control and make the difference between exceptions and assertions more clear.

* https://github.com/pithyless/invariant
* https://github.com/jorgemanrubia/solid_assert

Building Gems
=============
Guidelines for building/maintaining gem projects

* manage with [bundler](http://bundler.io/)
* choose a license
  - [GPL](https://www.gnu.org/licenses/gpl.html)
  - [MIT](https://en.wikipedia.org/wiki/MIT_License)
* setup support services and add badges to README
  - [RubyGems](http://rubygems.org/) (released version)
  - [CodeClimate](https://codeclimate.com) (code quality and test coverage)
  - [TravisCI](https://travis-ci.org) (continuous integration)
  - [InchCI](http://inch-ci.org) (documentation quality)
* gems to use by default:
  - [thor](http://whatisthor.com/) for CLI
  - [excon](https://github.com/excon/excon) for HTTP client
  - [rspec](http://rspec.info/) and [simplecov](https://github.com/colszowka/simplecov) for unit testing
  - [cucumber](https://cucumber.io/) for documentation and integration tests
  - [rubocop](https://github.com/bbatsov/rubocop) for Ruby static analysis
  - [capybara](https://teamcapybara.github.io/capybara/) for web browser testing
  - [webmock](https://github.com/webmock/webmock)
  - [fakefs](https://github.com/fakefs/fakefs)
  - [faker](https://github.com/stympy/faker)
  - [MulitJson](https://github.com/intridea/multi_json) + [oj](http://www.ohler.com/oj/)
  - [bundler-audit](https://rubygems.org/gems/bundler-audit)
  - [license_finder](https://rubygems.org/gems/license_finder)
  - [yardoc](https://yardoc.org/)
* sign the published gems
* publish the project code and related information
  - [Github](http://github.com/)
  - [GitLab](http://gitlab.com/)
  - [Ruby Gems](http://rubygems.org/)
  - [Ruby Toolbox](http://rubytoolbox.org)
  - [OpenHub](https://www.openhub.net/)
  - [Ruby LibHunt](https://ruby.libhunt.com/)
  - [rubydoc](https://www.rubydoc.info/)
  - [CucumberPro](https://app.cucumber.pro/)
* [code of conduct](http://contributor-covenant.org/)
* [changelog](http://keepachangelog.com/)
* [README](https://www.makeareadme.com/)
  - include an "Elsewhere" section which includes links to all the profiles,
    and places which this project can be found. Including its homepage and git
    repository
  - if appropriate, include an "Alternates" or "Similar Projects" section which
    describes other projects. This section can also describe the differences
    between projects, and aid user in choosing what to use
* contribution guidelines

When setting up TravisCI and CodeClimate, for a gem supporting Ruby 2.0 and up, the following .travis.yml can be used:

```yaml
language: ruby
os:
- linux
- osx
sudo: false
rvm:
  - 2.0
  - 2.1
  - 2.2
  - 2.3
  - 2.4
  - 2.5
  - ruby-head
matrix:
  allow_failures:
    - rvm: ruby-head
    - rvm: 2.0
      os: osx
    - rvm: 2.4
      os: osx
after_success:
  - bundle exec codeclimate-test-reporter
```

You will also need to add the CODECLIMATE\_REPO\_TOKEN to the TravisCI
environment variables, getting the value from the Code Climate repository
**Settings > Test Coverage**.

Deprecating a project
---------------------

Once a gem is no-longer maintained, it should be clearly marked as such.
Depending upon where the repository is hosted you should set as many of these
points as possible.

* update the title in the README.md to

```markdown
# :no_entry: DEPRECATED project name
```

* add the tags
  - unmaintained
  - deprecated
  - outdated
* mark the repository as archived

Reference
---------

* [shields.io](http://shields.io/) a service for generating more README shields
* https://philna.sh/blog/2017/07/12/two-tests-you-should-run-against-your-ruby-project-now/
* [How I Start - Let's Build a Gem Together! (Book Edition)](https://yukimotopress.github.io/start) by [Steve Klabnik](https://www.steveklabnik.com/)
* [Standard way of marking a github organization or repository as deprecated?](https://stackoverflow.com/questions/44376628/standard-way-of-marking-a-github-organization-or-repository-as-deprecated)
* [GemCheck](https://gemcheck.evilmartians.io/)
* there are some othe tools which automate similar checklist stuff
  - https://github.com/svenfuchs/gem-release
  - https://github.com/bkuhlmann/gemsmith

Ruby Blog Entires
=================
* [OPTIONS HTTP Method in Ruby on Rails](/blog/2014-11-26-options-http-method-in-ruby-on-rails)

Finally, "Is Ruby Dead?"
========================

A questions which seems to be continuously asked, but as Ruby continues to be a great
language to write in, and pretty alive I think the answer a solid no.

There is also a website which answers the question [Is Ruby Dead?](https://isrubydead.com/) And their answer is also **NO**.

But [rubyisdead.science](https://rubyisdead.science/) has graphs, fake quotes and "science". (And a tongue firmly planted in check.)
