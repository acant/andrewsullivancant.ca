---
:title: Notes For Indieweb Presentation at KWLUG
:tags: indieweb kwlug
:date: 2016-09-27
---
At the [KWLUG](http://kwlug.org/) meeting on [September 12 2016](http://kwlug.org/node/1031)
I gave a short introduction about the [IndieWeb](https://indieweb.org/). I do
not have any slides which are worth publishing but I will summarize what I presented
and provide some links.

I think that the 2 of the core principles of the IndieWeb are:
* owning your own data
* use visible data
(N.B.: There are [other principles too](https://indieweb.org/Principles) too.)

The key technologies to achieve this are:

* [microformats](http://microformats.org/), where semantic information is
  encoded directly into the HTML
* [webmention](https://indieweb.org/webmention) (e.g. a new simpler [pingback](https://indieweb.org/Pingback)), which allows notifications to be sent to other IndieWeb users

There are a variety of ways to combined these 2 ideas which are described
in the [building blocks page](https://indieweb.org/Category:building-blocks).

Now, with all of this Indiweb sites could talk to each other but are
effectively forming a new social network that has not external connections.
However, there are 2 ideas to address this:

* first, [POSSE](https://indieweb.org/POSSE) (Publish (on your) Own Site, Syndicate Elsewhere)
  - this is preferred because your content still lives primarily on your own site
  - but it is pushed out, so you can still communicate with your friends and
    family using the systems that they prefer
  - you may also want to [backfeed](https://indieweb.org/backfeed)
    comments, replies, RSVPs, etc. from the silos that you POSSE to. This allow
    your site to present a more full view of the activity surrounding yours
    posts
* second, [PESOS](https://indieweb.org/PESOS) (Publish Elsewhere, Syndicate (to your) Own Site)
  - the copy on your own site is not the canonical version with this method,
    which is not as good
  - still useful in making you less dependent upon the silo where you are
    publishing your content, as you always have another copy

# Getting started

## Get a name

* traditional DNS (short and long if possible)
* something distributed would be nice but not sufficient (e.g., [NameCoin](https://www.namecoin.info/))

## Choose a system to use

* [Wordpress plugin](https://wordpress.org/plugins/indieweb/)
* [Known](https://withknown.com/)
* static on [Gitlab pages](https://pages.gitlab.io/) (or [Github pages]())
* static on your own host or VPS
* pick an existing [Indieweb project](https://indieweb.org/projects)

## Follow some checklists to gauge your progress

* https://indieweb.org/Getting_Started
* https://indieweb.org/IndieMark
* https://aaronjorbin.github.io/indiemark-score/
* https://indieweb.org/SWAT0

# Example scenarios for publishing posts

Here are the simple examples that I walked through in my presentation:

## Posting an article about the KWLUG meeting

```html
<article class="h-entry">
  <header>
    <h1 class="p-name">
      About KWLUG September 2016
    </h1>
    <p>
      By <a class="p-author h-card" href="http://andrewsullivancant.ca">Andrew Sullivan Cant</a>
      on <time class="dt-published" datetime="2016-09-12 23:30:00">2016-09-12</time>.
    </p>
  </header>
  <section class="p-summary">
    <p>
      Here are some notes about <a href="http://kwlug.org/node/1031">KWLUG September 2016</a>.
      Presenters include <href="/">me</href>, <a href="http://sobac.com/bjonkman/">Bob</href>, Kirk and more.
    </p>
  </section>
  <section class="e-content">
    <p>And here I can say all the things.</p>
  </section>
  <footer>
    <a class="p-url" href="http://andrewsullivancant.ca/2016/09/13/articles/about_kwlug_september_2016">
      permalink
    </a>
  </footer>
</article>
```

## Steps to publish it

* I post the article on my site at:
  - http://andrewsullivancant.ca/2016/09/13/articles/about_kwlug_september_2016
* send webmention to KWLUG
* send webmention to each of the participants
* KWLUG will post the link on the meeting page
* personal pages may or may not do something with it

## Making a comment on the meeting

```html
<article class="h-entry">
  <a href="http://kwlug.org/node/1031" rel="in-reply-to" class="u-in-reply-to">
    KWLUG September 2016
  </a>
  <div>Hi I am a comment.</div>
</article>
```

## Steps to publish it
* I post a comment on my own page at:
  - http://andrewsullivancant.ca/2016/09/03/e0b4dd86-7922-11e6-bb6d-00262d9868a8
* publishing system sends a webmention to KWLUG
* KWLUG moderator reviews my comment and approves it
* KWLUG displays my comment in its own page


