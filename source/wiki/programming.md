---
title: Programming
---

# Code Readability
* [line length](https://en.wikipedia.org/wiki/Line_length)
  - [Line length and readability: speed vs. user experience](https://samnabi.com/blog/line-length-and-readability-speed-vs-user-experience) cites some more research about this
  - [Why 80 columns](https://corecursive.com/why-80-columns/)
    * [discussion on Lobster.rs](https://lobste.rs/s/kofzic/why_still_80_columns)
  - [Is the 80 character line limit still relevant?](https://richarddingwall.name/2008/05/31/is-the-80-character-line-limit-still-relevant/)
    * [discussion on HackerNews](https://news.ycombinator.com/item?id=42116051)
* [vertical alignment](https://en.wikipedia.org/wiki/Programming_style#Vertical_alignment)
  - https://lawler.io/scrivings/long-form-websites-and-typography/
* find out about syntax highlighting

# Boring Technologies

Various people have expressed this in various ways. The general idea, I think,
I choosing to limit yourself in some areas so you can put more time and energy
into other areas.

The ideas of [convention over configuration](https://en.wikipedia.org/wiki/Convention_over_configuration) and "sensible defaults" are similar ideas.

## External Links
* https://mcfunley.com/choose-boring-technology
* https://boringtechnology.club/

# External Links

* [Software Engineering Tips for CLMS Students](https://catball.dev/clms_swe_tips/)
* [Massively increase your productivity on personal projects with comprehensive documentation and automated tests](https://simonwillison.net/2022/Nov/26/productivity/)
* [Software Engineering Illustrated: I'm just trying to change this lightbulb](https://www.mikesteder.com/engineering/management/gifs/software/illustrated/2014/12/31/software-engineering-illustrated-im-just-trying-to-change-this-lightbulb.html)
* [Fighting Distraction with Unit Tests](https://matthewc.dev/musings/unit-tests/)
