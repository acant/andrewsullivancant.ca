---
title: Free Libre Open Source Software
---

* [Waterloo Free Software](/wiki/waterloo_free_software_portal)
* [Waterloo FLOSS Club](/wiki/waterloo_floss_club)

# Events in Canada

* [Pycon](https://pycon.ca/)

# Open Source Business Models and Sustainability

Noting things that have been said about sustaining FLOSS projects.

* https://medium.com/@nayafia/how-i-stumbled-upon-the-internet-s-biggest-blind-spot-b9aa23618c58
* https://medium.com/@nayafia/open-source-infrastructure-the-q-a-a44615861944
* https://medium.com/@nayafia/open-source-was-worth-at-least-143m-of-instagram-s-1b-acquisition-808bb85e4681#.w58lpwmps
* https://medium.com/@nayafia/we-re-in-a-brave-new-post-open-source-world-56ef46d152a3#.1yyreuae7
* https://opensource.com/article/18/1/how-start-open-source-program-your-company
* https://hbswk.hbs.edu/item/the-hidden-benefit-of-giving-back-to-open-source-software
* https://www.fordfoundation.org/about/library/reports-and-studies/roads-and-bridges-the-unseen-labor-behind-our-digital-infrastructure
* https://www.indiehackers.com/post/how-i-earn-a-living-selling-my-open-source-software-476f6bb07e
* https://matt.life/writing/the-asymmetry-of-open-source
* https://shkspr.mobi/blog/2021/12/book-review-working-in-public-the-making-and-maintenance-of-open-source-software-by-nadia-eghbal/
* https://blog.scottlogic.com/2021/12/20/open-source-sustainability.html
* https://shkspr.mobi/blog/2022/01/how-do-artists-get-paid/
* [How to pay professional maintainers](https://words.filippo.io/pay-maintainers/)
* [Support open source that you use by paying the maintainers to talk to your team](https://simonwillison.net/2022/Feb/23/support-open-source/)
* [Selling my own GPL software part 3, prior art (existing GPL software for sale)](https://raymii.org/s/blog/Existing_GPL_software_for_sale.html)
* [How to support open-source software and stay sane](https://www.nature.com/articles/d41586-019-02046-0)
  - this references academic funding sources in the US and UK, I wonder what
    the equivalents in Canada would be
* [The Asymmetry of Open Source](https://matt.life/writing/the-asymmetry-of-open-source)
* [I'm Now a Full-time Professional Open Source Maintainer](https://words.filippo.io/full-time-maintainer/)

## Co-op free-software consultancies

* [Igalia](https://www.igalia.com/)([wikipedia](https://en.m.wikipedia.org/wiki/Igalia))
  is a worker co-operative consultancy which does work in free software
  - https://thenewstack.io/igalia-the-open-source-powerhouse-youve-never-heard-of/
* [Agaric Co-op](https://agaric.coop/)


# Open Source Program Office (OSPO)

A distinct department within a corporation which facilitates using and
contributing to open source.

* [TODO Group](https://todogroup.org/)
  - https://en.wikipedia.org/wiki/Linux_Foundation#TODO
  - https://www.zdnet.com/article/marrying-open-source-and-the-enterprise/
* https://developer.gs.com/blog/posts/goldmansachs-ospo-one-year-in

# Sources of funding

* [Code for Canada Fellowships](http://www.codefor.ca/apply)
* [Google Summer of Code](https://summerofcode.withgoogle.com/)
* [Mozilla Open Source Support(MOSS)](https://www.mozilla.org/en-US/moss/)
* [Outreachy](https://www.outreachy.org/)
* [OSS.fund](https://www.oss.fund/) list of monetiziation platforms for Open
    Source
* [Grant for the Web](https://www.grantfortheweb.org/)
* [Indeed FOSS Contributor Fund](http://opensource.indeedeng.io/FOSS-Contributor-Fund/)
* [OpenCollective](https://opencollective.com/)
* [FundOSS](https://fundoss.org/)
* [List of opportunities, grants, fellowship programs, contests and things like that for young ambitious people](https://harshitaarora.com/2019/01/02/list-of-opportunities-grants-fellowship-programs-contests-and-things-like-that-for-young-ambitious-people/)
* [List of Microgrants](https://github.com/nayafia/microgrants)
* [SOS Rewards](https://sos.dev/)
* [OSS Virtual Incubator](https://github.com/PlaintextGroup/oss-virtual-incubator)
* [List of Open source Internship Programs](https://github.com/deepanshu1422/List-Of-Open-Source-Internships-Programs)
* [FOSS Jobs](https://github.com/fossjobs/fossjobs/wiki/resources)
* [nlnet Foundation - Alternative Funding Sources](https://nlnet.nl/foundation/network.html)
* [25+ Paid Open Source Programs and Internships](https://blog.commclassroom.org/25-paid-open-source-programs-and-internships)
* [Preparing for the wave of open source funding](https://sethmlarson.dev/blog/preparing-for-the-wave-of-open-source-funding)
* [A handy guide to financial support for open source](https://github.com/nayafia/lemonade-stand)
* [Prototype Fund](https://prototypefund.de/en/)
  - fund for Open Source developers in Germany
* [Sovereign Tech Fund](https://sovereigntechfund.de/de/)
  - [Sovereign Tech Fund Feasibility Study](https://sovereigntechfund.de/files/SovereignTechFund_Machbarkeitsstudie_en.pdf)
  - discussed on the Open Source Security Podcast [episode 368](https://opensourcesecuritypodcast.libsyn.com/episode-368-the-sovereign-tech-fund-with-fiona-krakenbrger)
* [Open Source Pledge](https://opensourcepledge.com/)
  - https://lucumr.pocoo.org/2024/9/19/open-source-tax/

## Monetizing content

If you write a project and write about a project, monetizing that content could
also be a useful way to sustain that work:

* ads and affiliate links
  - [Ethical Affiliate Marketing](https://www.autodidacts.io/ethical-affiliate-marketing/)
* donations/subscriptions/patronage
  - example of donation from [The Marginalian(Brain Pickings)](https://www.themarginalian.org/donate/)
  - [Patreon](https://www.patreon.com/home)
* https://webmonetization.org/
* https://interledger.org/
* https://coil.com/
* https://hacks.mozilla.org/2020/03/web-monetization-coil-and-firefox-reality/
* [Ideas to monetize your side projects.](https://www.brunoquaresma.dev/ideas-to-monetize-your-side-project/)

# Getting started

There are various projects which are working to make it easier for new
contributors. The following contains projects which mark issues beginner
friendly, list of those projects, or tools to help find those projects.

* [Awesome First PR Opportunities](https://github.com/MunGell/awesome-for-beginners)
* [OpenHatch](https://openhatch.org/)
* [Up for grabs](http://up-for-grabs.net/#/)
* [Your First PR](https://yourfirstpr.github.io/)

## Advice and discussions

* [6 Staring Points for Open Source Beginners](https://opensource.com/life/16/1/6-beginner-open-source)
* [Request For Commits – Episode #10: Finding New Contributors](https://changelog.com/rfc/10)

# Other tools for finding issues to work on

There are tools for finding things to work on in FLOSS projects, but are not
specifically for beginners.

* [BugsAhoy](http://www.joshmatthews.net/bugsahoy/), which search open issues
  in [Mozilla](https://www.mozilla.org/) projects
* [Code Triage](https://www.codetriage.com/)
