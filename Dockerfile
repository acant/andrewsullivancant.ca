FROM ruby:2.5

RUN apt-get update -yqqq

RUN apt-get install -y nodejs

WORKDIR /app

COPY Gemfile Gemfile.lock ./

RUN bundle install

EXPOSE 4567
